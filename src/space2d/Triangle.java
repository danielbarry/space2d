package space2d;

import java.awt.Color;

public class Triangle extends Shape{
    private static final float MATHC = 0.8660254037844f;
    private static final float MATHD = 0.5f;
    
    public Triangle(float offsetX, float offsetY, float radius, Color color){
        super(offsetX, offsetY, new float[]{0, MATHC * radius, -MATHC * radius, 0}, new float[]{-radius, MATHD * radius, MATHD * radius, -radius}, 4, color);
    }
}