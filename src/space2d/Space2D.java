package space2d;

import javax.swing.SwingUtilities;;

public class Space2D{
    private Game game;
    
    public static void main(String[] args){ new Space2D(args); }
    
    public Space2D(String[] args){ initWindow(); }
    
    private void initWindow(){
        SwingUtilities.invokeLater(game = new Game());
        game.simpleUpdate();
    }
}