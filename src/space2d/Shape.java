package space2d;

import java.awt.Color;

public class Shape{
    private static final float OFFSETANGLE = 2.3560f;
    
    public static int width, height;
    private float origOffsetX, origOffsetY;
    private int offsetX, offsetY;
    private Color color;
    private float[] origLinesX, origLinesY;
    private int[] linesX, linesY;
    private int linesAmount;
    private boolean rotate = false;
    private float angle = -OFFSETANGLE;
    
    //Optional shape properties
    private boolean boundingBox = false;
    private float x1, y1, x2, y2;
    
    public Shape(float[] linesX, float[] linesY, int linesAmount, Color color){
        simpleSetOffset(0.5f, 0.5f);
        setLinesX(linesX);
        setLinesY(linesY);
        setLinesAmount(linesAmount);
        setColor(color);
    }
    
    public Shape(float offsetX, float offsetY, float[] linesX, float[] linesY, int linesAmount, Color color){
        simpleSetOffset(offsetX, offsetY);
        setLinesX(linesX);
        setLinesY(linesY);
        setLinesAmount(linesAmount);
        setColor(color);
    }
    
    public void simpleSetOffset(float offsetX, float offsetY){
        origOffsetX = offsetX;
        origOffsetY = offsetY;
        this.offsetX = (int)(offsetX * width);
        this.offsetY = (int)(offsetY * height);
    }
    
    public void setOffset(float offsetX, float offsetY){
        simpleSetOffset(offsetX, offsetY);
        update();
    }
    
    private void simpleSetLinesX(float[] linesX){
        int[] linesXTemp = new int[linesX.length];
        for(int x=0; x<linesX.length; x++){
            linesXTemp[x] = (int)(linesX[x] * width) + offsetX;
        }
        this.linesX = linesXTemp;
    }
    
    public void setLinesX(float[] linesX){
        origLinesX = linesX;
        simpleSetLinesX(linesX);
    }
    
    private void simpleSetLinesY(float[] linesY){
        int[] linesYTemp = new int[linesY.length];
        for(int x=0; x<linesY.length; x++){
            linesYTemp[x] = (int)(linesY[x] * height) + offsetY;
        }
        this.linesY = linesYTemp;
    }
    
    public void setLinesY(float[] linesY){
        origLinesY = linesY;
        simpleSetLinesY(linesY);
    }
    
    public void setColor(Color color){ this.color = color; }
    
    public void setBoundingBox(float x1, float y1, float x2, float y2){
        boundingBox = true;
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
    
    public void setRotation(boolean rotate){ this.rotate = rotate; }
    
    public void setAngle(float angle){
        rotate = true;
        this.angle = angle;
        update();
    }
    
    public void removeBoundingBox(){ boundingBox = false; }
    
    public boolean hasBoundingBox(){ return boundingBox; }
    
    public float[] getBoundingBox(){
        float[] boundingBox = new float[4];
        boundingBox[0] = x1 + origOffsetX;
        boundingBox[1] = y1 + origOffsetY;
        boundingBox[2] = x2 + origOffsetX;
        boundingBox[3] = y2 + origOffsetY;
        return boundingBox;
    }
    
    public void setLinesAmount(int linesAmount){ this.linesAmount = linesAmount; }
    
    public float getOrigOffsetX(){ return origOffsetX; }
    
    public float getOrigOffsetY(){ return origOffsetY; }
    
    public int[] getLinesX(){ return linesX; }
    
    public int[] getLinesY(){ return linesY; }
    
    public float[] getOrigLinesX(){ return origLinesX; }
    
    public float[] getOrigLinesY(){ return origLinesY; }
    
    public int getLinesAmount(){return linesAmount;  }
    
    public Color getColor(){ return color; }
    
    public float getAngle(){ return angle; }
    
    public void update(){
        simpleSetOffset(origOffsetX, origOffsetY);
        if(rotate){
            float[][] temp = rotateLines();
            simpleSetLinesX(temp[0]);
            simpleSetLinesY(temp[1]);
        }else{
            simpleSetLinesX(origLinesX);
            simpleSetLinesY(origLinesY);
        }
    }
    
    public float[][] rotateLines(){
        float[][] temp = new float[2][linesAmount];
        float angle = this.angle + OFFSETANGLE;
        double resultCos = Math.cos(angle);
        double resultSin = Math.sin(angle);
        for(int x=0; x<linesAmount; x++){
            temp[0][x] = (float)((origLinesX[x] * resultCos) - (origLinesY[x] * resultSin));
            temp[1][x] = (float)((origLinesX[x] * resultSin) + (origLinesY[x] * resultCos));
        }
        return temp;
    }
    
    public boolean interesection(Shape shape){
        if(hasBoundingBox()&&shape.hasBoundingBox()){
            float[] boundingBox1 = getBoundingBox();
            float[] boundingBox2 = shape.getBoundingBox();
            if(checkPoint(boundingBox1, boundingBox2[0], boundingBox2[1]))return true;
            if(checkPoint(boundingBox1, boundingBox2[0], boundingBox2[3]))return true;
            if(checkPoint(boundingBox1, boundingBox2[2], boundingBox2[3]))return true;
            if(checkPoint(boundingBox1, boundingBox2[2], boundingBox2[1]))return true;
            
            if(checkPoint(boundingBox2, boundingBox1[0], boundingBox1[1]))return true;
            if(checkPoint(boundingBox2, boundingBox1[0], boundingBox1[3]))return true;
            if(checkPoint(boundingBox2, boundingBox1[2], boundingBox1[3]))return true;
            if(checkPoint(boundingBox2, boundingBox1[2], boundingBox1[1]))return true;
        }
        return false;
    }
    
    public boolean checkPoint(float[] box, float x, float y){ return x>=box[0]&&x<=box[2]&&y>=box[1]&&y<=box[3]; }
}