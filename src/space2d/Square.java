package space2d;

import java.awt.Color;

public class Square extends Shape{
    public Square(float offsetX, float offsetY, float width, float height, Color color){
        super(offsetX, offsetY, new float[]{-width/2f, -width/2f, width/2f, width/2f, -width/2f}, new float[]{-height/2f, height/2f, height/2f, -height/2f, -height/2f}, 5, color);
    }
}