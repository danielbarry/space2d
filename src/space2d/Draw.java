package space2d;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Draw extends JPanel implements ComponentListener, KeyListener{
    private JFrame jFrame;
    private ArrayList<Shape> shapes = new ArrayList<Shape>();
    private boolean resize = false;
    
    public Draw(){}
    
    public void init(String title){
        this.addComponentListener(this);
        this.setFocusable(true);
        this.addKeyListener(this);
        jFrame = new JFrame(title);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setSize(320, 200);
        jFrame.setContentPane(this);
        this.setBackground(Color.BLACK);
        jFrame.setVisible(true);
        updateShapesSize();
    }
    
    public void setTitle(String title){ jFrame.setTitle(title); }
    
    @Override
    public void paintComponent(Graphics graphics){
        super.paintComponent(graphics);
        if(resize)updateShapesSize();
        for(Shape shape : shapes){
            graphics.setColor(shape.getColor());
            graphics.drawPolyline(shape.getLinesX(), shape.getLinesY(), shape.getLinesAmount());
        }
    }
    
    @Override
    public void componentResized(ComponentEvent e){ resize = true; }
    
    @Override
    public void componentHidden(ComponentEvent e){}
    
    @Override
    public void componentMoved(ComponentEvent e){}
    
    @Override
    public void componentShown(ComponentEvent e){}
    
    public void addShape(Shape shape){ shapes.add(shape); }
    
    public boolean removeShape(Shape shape){ return shapes.remove(shape); }
    
    private void updateShapesSize(){
        Shape.width = this.getWidth();
        Shape.height = this.getHeight();
        for(Shape shape : shapes)shape.update();
        resize = false;
    }
    
    public void refresh(){
        validate();
        repaint();
    }
    
    @Override
    public void keyPressed(KeyEvent e){}

    @Override
    public void keyReleased(KeyEvent e){}
    
    @Override
    public void keyTyped(KeyEvent e){}
}