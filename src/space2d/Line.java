package space2d;

import java.awt.Color;

public class Line extends Shape{
    public Line(float x1, float y1, float x2, float y2, Color color){
        super(new float[]{x1, x2}, new float[]{y1, y2}, 2, color);
    }
}