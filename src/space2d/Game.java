package space2d;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.Random;

public class Game extends Draw implements Runnable{
    private static final double TOPSPEED = 0.002f, ACCEL = 0.0001f, DEACCEL = 0.00002f, SHOOTSPEED = 0.004f;
    private boolean running = true;
    private long score = 0;
    private float plyrSpeed = 0f;
    private boolean plyrUp = false, plyrLeft = false, plyrRight = false, plyrShoot = false;
    private Shape plyr = new Shape(new float[]{-0.02f, 0f, 0.02f}, new float[]{0.02f, -0.02f, 0.02f}, 3, Color.BLUE);
    private Bullet[] bullets = new Bullet[10];
    private Astroid[] astroids = new Astroid[10];
    private static Random rand = new Random();
    
    public Game(){
        plyr.setBoundingBox(-0.02f, -0.02f, 0.02f, 0.02f);
        addShape(plyr);
    }
    
    public void simpleUpdate(){
        while(running){
            //Handle Kep Presses
            if(plyrUp){
                if(plyrSpeed<TOPSPEED)plyrSpeed += ACCEL;
            }else{
                if(plyrSpeed>0)plyrSpeed -= DEACCEL;
                else plyrSpeed = 0;
            }
            if(plyr.getOrigOffsetX()<0)plyr.setOffset(1f + plyr.getOrigOffsetX(), plyr.getOrigOffsetY());
            if(plyr.getOrigOffsetY()<0)plyr.setOffset(plyr.getOrigOffsetX(), 1f + plyr.getOrigOffsetY());
            if(plyr.getOrigOffsetX()>1)plyr.setOffset(1f - plyr.getOrigOffsetX(), plyr.getOrigOffsetY());
            if(plyr.getOrigOffsetY()>1)plyr.setOffset(plyr.getOrigOffsetX(), 1f - plyr.getOrigOffsetY());
            float resultCos = (float)(Math.cos(plyr.getAngle()) * plyrSpeed);
            float resultSin = (float)(Math.sin(plyr.getAngle()) * plyrSpeed);
            plyr.setOffset(plyr.getOrigOffsetX() + resultCos - resultSin, plyr.getOrigOffsetY() + resultSin + resultCos);
            if(plyrLeft)plyr.setAngle(plyr.getAngle() - 0.1f);
            if(plyrRight)plyr.setAngle(plyr.getAngle() + 0.1f);
            
            float shootCos = (float)(Math.cos(plyr.getAngle()) * SHOOTSPEED);
            float shootSin = (float)(Math.sin(plyr.getAngle()) * SHOOTSPEED);
            float shootX = -0.5f + plyr.getOrigOffsetX();
            float shootY = -0.5f + plyr.getOrigOffsetY();
            if(plyrShoot){
                for(int x=0; x<bullets.length; x++){
                    if(bullets[x]==null){
                        Bullet bullet = new Bullet(shootX, shootY, shootCos - shootSin, shootSin + shootCos, Color.CYAN);
                        bullets[x] = bullet;
                        bullet.setBoundingBox(-0.0000001f, -0.0000001f, 0.0000001f, 0.0000001f);
                        addShape(bullet);
                        break;
                    }
                }
                plyrShoot = false;
            }
            
            //Move bullets
            for(int x=0; x<bullets.length; x++){
                if(bullets[x]!=null){
                    if(bullets[x].move()){
                        //Remove because off screen
                        removeShape(bullets[x]);
                        bullets[x] = null;
                    }
                }
            }
            
            //Create astroids
            shootCos = -shootCos / 4f;
            shootSin = -shootSin / 4f;
            for(int x=0; x<astroids.length; x++){
                if(astroids[x]==null){
                    float astX = 0;
                    float astY = 0;
                    if(rand.nextInt(2)==0){
                        astX = rand.nextInt(2);
                        astY = rand.nextFloat();
                    }else{
                        astX = rand.nextFloat();
                        astY = rand.nextInt(2);
                    }
                    float size = rand.nextFloat() / 10f;
                    Astroid astroid = new Astroid(astX, astY, size, shootCos - shootSin, shootSin + shootCos, Color.DARK_GRAY);
                    astroids[x] = astroid;
                    size = size / 2f;
                    astroid.setBoundingBox(-size, -size, size, size);
                    addShape(astroid);
                    break;
                }
            }
            
            //Move astroids
            for(int x=0; x<astroids.length; x++){
                if(astroids[x]!=null){
                    if(astroids[x].move()){
                        //Remove because off screen
                        removeShape(astroids[x]);
                        astroids[x] = null;
                    }else{
                        //Check for astroid collision
                        for(int y=0; y<bullets.length; y++){
                            if(astroids[x]!=null&&bullets[y]!=null){
                                if(astroids[x].interesection(bullets[y])){
                                    setTitle("Space2D -- Score:" + ++score);
                                    removeShape(bullets[y]);
                                    bullets[y] = null;
                                    removeShape(astroids[x]);
                                    astroids[x] = null;
                                }
                            }
                        }
                        //Check player collision
                        if(astroids[x]!=null){
                            if(astroids[x].interesection(plyr)){
                                astroids[x].setColor(Color.MAGENTA);
                                plyr.setColor(Color.RED);
                                running = false;
                            }
                        }
                    }
                }
            }
            
            //Refresh the Graphics
            refresh();
            
            //Wait to redraw
            try{ Thread.sleep(16, 666); }
            catch(Exception e){}
        }
    }
    
    @Override
    public void run(){ init("Space2D"); }
    
    @Override
    public void keyPressed(KeyEvent e){
        if(e.getKeyChar()=='w')plyrUp = true;
        if(e.getKeyChar()=='a')plyrLeft = true;
        if(e.getKeyChar()=='d')plyrRight = true;
        if(e.getKeyChar()==' ')plyrShoot = true;
}

    @Override
    public void keyReleased(KeyEvent e){
        if(e.getKeyChar()=='w')plyrUp = false;
        if(e.getKeyChar()=='a')plyrLeft = false;
        if(e.getKeyChar()=='d')plyrRight = false;
        if(e.getKeyChar()==' ')plyrShoot = false;
    }
    
    @Override
    public void keyTyped(KeyEvent e){
        if(e.getKeyChar()==KeyEvent.VK_ESCAPE)System.exit(0);
    }
}