package space2d;

import java.awt.Color;

public class Astroid extends Square{
    private float velX, velY;
    
    public Astroid(float offsetX, float offsetY, float radius, float velX, float velY, Color color){
        super(offsetX, offsetY, radius, radius, color);
        this.velX = velX;
        this.velY = velY;
    }
    
    public boolean move(){
        float offsetX = getOrigOffsetX();
        float offsetY = getOrigOffsetY();
        setOffset(offsetX + velX, offsetY + velY);
        if(offsetX<0||offsetX>1||offsetY<0||offsetY>1)return true;
        else return false;
    }
}