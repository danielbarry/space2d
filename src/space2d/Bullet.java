package space2d;

import java.awt.Color;

public class Bullet extends Line{
    private float velX, velY;
    
    public Bullet(float x1, float y1, float velX, float velY, Color color){
        super(x1, y1, x1 + velX, y1 + velY, color);
        this.velX = velX;
        this.velY = velY;
    }
    
    public boolean move(){
        float offsetX = getOrigOffsetX();
        float offsetY = getOrigOffsetY();
        setOffset(offsetX + velX, offsetY + velY);
        if(offsetX<0||offsetX>1||offsetY<0||offsetY>1)return true;
        else return false;
    }
}